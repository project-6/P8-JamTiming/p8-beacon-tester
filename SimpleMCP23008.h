//
// Created by vlchung on 26/9/20.
//

#ifndef HALLTEST_SIMPLEMCP23008_H
#define HALLTEST_SIMPLEMCP23008_H

#ifdef ARDUINO_attiny
    #include <TinyWireM.h>
    #define TheWire TinyWireM
    #define TINYWIRE
#else
    #include <Wire.h>
    #define TheWire Wire
    #undef TINYWIRE
#endif

class SimpleMCP23008 {
public:
    SimpleMCP23008(uint8_t address = 0x20);
    ~SimpleMCP23008();
    void begin();
    void setDDR(uint8_t ddr);
    void setInversion(uint8_t inv);
    void setPullUps(uint8_t pullUps);
    uint8_t read();
    void write(uint8_t);
private:
    inline void mySend(uint8_t byte);
    // a kludge to make the code easier by not needing to pepper it with casts to uint8_t
    uint8_t mAddress;
    uint8_t mDDR;
};

#endif //HALLTEST_SIMPLEMCP23008_H
