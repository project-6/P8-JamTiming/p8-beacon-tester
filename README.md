# Timing Beacon Tester
This is designed to be used to test the P8 (aka "Jam Electronic Timer") timing
beacon, which is 5 LEDs which "count" the 5 second before the start of a Jam.

![](fritzing.png)

When connected correctly to a timing beacon, it should look like this:

![](test.gif)

