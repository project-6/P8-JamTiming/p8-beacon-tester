#include "SimpleMCP23008.h"

SimpleMCP23008 testUnit(0x27);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  testUnit.begin();
  testUnit.setDDR(0x0);
//  testUnit.write(0x00);
}


uint8_t thePin = 0;
long lastChange = 0;
void loop() {
  // put your main code here, to run repeatedly:
    if (millis() - lastChange > 125) {
        Serial.println(thePin);
        testUnit.write(thePin++);
        lastChange = millis();
        if (thePin >= 32)
          thePin = 0;
    }
}
